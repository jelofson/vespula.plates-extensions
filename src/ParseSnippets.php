<?php

namespace Vespula\PlatesExtensions;

use League\Plates\Engine;

class ParseSnippets extends Base 
{
    
	protected $viewPath;
    
    public function register(Engine $engine)
	{

        if (! $this->viewPath) {
            $this->viewPath = dirname($engine->getDirectory()) . '/Snippets/Views';
        }
        $engine->registerFunction('parseSnippets', [$this, 'parseSnippets']);
        
        $engine->addFolder('snippets', $this->viewPath);
	}
    
    public function setViewPath($path)
    {
        $this->viewPath = $path;
    }
    
    public function parseSnippets($text)
    {
        $text = preg_replace_callback(
			"{(<(div|p)>[\s]*)?\[snippet\](.*)\[\/snippet\]([\s]*<\/(div|p)>)?}",
			array($this, 'parseText'), $text);

		return $text;
    }
    
    protected function parseText($matches)
	{
        $params = [];
		$string = html_entity_decode($matches[3]);
		parse_str($string, $params);
		if (! $params['name']) {
			return;
		}
        
        $inflector = $this->container->get('inflector');
        $className = '\\Cms\Snippets\\' . $inflector->classify($params['name']); 
        
        if (! class_exists($className)) {
            return;
        }
        
        $snippet = new $className($this->template, $this->container);
        $snippet->setParams($params);
        $html = $snippet->__invoke();

		
		// if wrapped in a tag, skip the tag
		if (! empty($matches[2])) {
			if (substr($html, 0, 7) != "<script") {
				$html = "<div>\n" . $html . "\n</div>\n";
			}
			
		}

		return $html;
	}
}