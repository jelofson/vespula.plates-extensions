<?php

namespace Vespula\PlatesExtensions;

use League\Plates\Engine;

class Paginator extends Base 
{
    
	public function register(Engine $engine)
	{
		$engine->registerFunction('paginate', [$this, 'paginate']);
	}


    public function paginate($path, $content, $page = 1, $tag = 'h2')
    {
        $preamble = null;
        $headings = [];
        $matches = [];
        preg_match_all("/<$tag\b[^>]*>(.*)<\/$tag>/sxU", $content, $matches, PREG_OFFSET_CAPTURE);
        
        if (! $matches[0]) {
            return $content;
        }
        
        $page = (int) $page;
        
        if (! $page) {
            $page = 1;
        }
        
        if ($page > count($matches[0])) {
            $page = count($matches[0]);
        }
        
        $headings = $this->getRawHeadings($matches[1]);
        $preamble = $this->getPreamble($content, $page, $matches[0]);
        $text = $this->getPageText($content, $page, $matches[0]);

        $toc = $this->buildToc($path, $headings, $page);
        $footer = $this->buildFooter($path, $page, $headings);
        
        $html = [
            $preamble,
            $toc,
            '<hr/>',
            $text,
            '<hr/>',
            $footer
        ];
        
        return implode(PHP_EOL, $html);
 
    }
    
    protected function getRawHeadings($data)
    {
        $headings = [];
        foreach ($data as $headingInfo) {
            $headings[] = $headingInfo[0];
        }
        return $headings;
    }
    
    protected function getPreamble($content, $page, $data)
    {
        // Position of first header
        // Anything before this is "preamble"
        $first = $data[0][1];
        $preamble = null;
        if ($first > 0 && $page == 1) {
            $preamble = substr($content, 0, $first);
        }
        
        return $preamble;
        
        
    }
    
    protected function getPageText($content, $page, $data)
    {
        // Get Position of the current page text
        $start = $data[$page-1][1];
        
        $pages = count($data);
        // on last page. show from start to the end.
        if ($page == $pages) {
            $text = substr($content, $start);
            return $text;
        }
        
        $length = $data[$page][1]-$start;
        $text = substr($content, $start, $length);
        return $text;
    }
    
    protected function buildToc($path, $headings, $page)
    {
        $html = [];
        $html[] = '<ul>';
        foreach ($headings as $key=>$heading) {
            if ($key+1 == $page) {
                $html[] = '    <li>' . $heading . '</li>';
            } else {
                $href = $path . '/' . ($key+1);
                $html[] = '    <li><a href="' . $href . '">' . $heading . '</a></li>';
            }
        }
        $html[] = '</ul>';
        return implode(PHP_EOL, $html);
    }
    
    protected function buildFooter($path, $page, $headings)
    {
        $prev = '<div class="col-lg-6"></div>';
        $next = '<div class="col-lg-6"></div>';
        if ($page > 1) {
            $href = $path . '/' . ($page-1);
            $prev = '    <div class="col-lg-6"><a href="' . $href . '">' . $headings[$page-1] . '</a></div>';
        }
        if ($page < count($headings)) {
            $href = $path . '/' . ($page+1);
            $next = '    <div class="col-lg-6 text-right"><a href="' . $href . '">' . $headings[$page] . '</a></div>';
        }
        
        $html = [
            '<div class="row">',
            $prev,
            $next,
            '</div>'
        ]; 
        return implode(PHP_EOL, $html);
    }
}
