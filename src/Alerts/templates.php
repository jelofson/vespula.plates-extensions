<?php

$bootstrap_info = '<div class="alert alert-info alert-dismissible" role="alert">' .
                  '     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' .
                  '     %s' .
                  '</div>';
$bootstrap_error = '<div class="alert alert-danger alert-dismissible" role="alert">' .
                   '     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' .
                   '     %s' .
                   '</div>';

$foundation_info = '<div class="callout primary" data-closable>' .
                    '    %s' .
                    '    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>' .
                    '       <span aria-hidden="true">&times;</span>' .
                    '    </button>' .
                    '</div>';
$foundation_error = '<div class="callout alert" data-closable>' .
                    '    %s' .
                    '    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>' .
                    '       <span aria-hidden="true">&times;</span>' .
                    '    </button>' .
                    '</div>';

$bs5_info = '<div class="alert alert-info alert-dismissible" role="alert">' .
                  '     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' .
                  '     %s' .
                  '</div>';
$bs5_error = '<div class="alert alert-danger alert-dismissible" role="alert">' .
                   '     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                   </button>' .
                   '     %s' .
                   '</div>';
                    
$default_info = '<div style="padding: 5px; border: 1px solid #ccc; background-color: #A0D3E8; margin-bottom: 10px;">%s</div>';
$default_error = '<div style="padding: 5px; border: 1px solid #ccc; background-color: #fcc; margin-bottom: 10px;">%s</div>';

return [
    'bootstrap'=>[
        'info'=>$bootstrap_info,
        'error'=>$bootstrap_error
    ],
    'foundation'=>[
        'info'=>$foundation_info,
        'error'=>$foundation_error,
    ],
    'gcweb'=>[
        'info'=>$bootstrap_info,
        'error'=>$bootstrap_error,
    ],
    'gcwu'=>[
        'info'=>$bootstrap_info,
        'error'=>$bootstrap_error,
    ],
    'default'=>[
        'info'=>$default_info,
        'error'=>$default_error,
    ],
    'bs5'=>[
        'info'=>$bs5_info,
        'error'=>$bs5_error,
    ]
];


