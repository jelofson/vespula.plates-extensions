<?php

namespace Vespula\PlatesExtensions;

use League\Plates\Engine;

class Breadcrumb extends Base 
{

    protected $templates = [];
    protected $theme;
    protected $crumbs = [];
    protected $container_tag = 'ol';
    protected $container_class = 'breadcrumb';
    protected $item_tag = 'li';
    protected $item_class = '';
    protected $active_class = 'active';

    public function register(Engine $engine)
    {
        $this->engine = $engine;
        $engine->registerFunction('breadcrumb', [$this, 'getSelf']);
    }

    public function setContainerTag($tag)
    {
        $this->container_tag = $tag;
    }

    public function setContainerClass($class)
    {
        $this->container_class = $class;
    }

    public function setItemTag($tag)
    {
        $this->item_tag = $tag;
    }

    public function setItemClass($class)
    {
        $this->item_class = $class;
    }

    public function setActiveClass($class)
    {
        $this->active_class = $class;
    }

    public function getSelf()
    {
        return $this;
    }

    public function add($text, $href = null)
    {
        $crumb = [
            'text' => $text,
            'href' => $href
        ];

        array_push($this->crumbs, $crumb);

        return $this;
    }

    public function getCrumbs()
    {
        return $this->crumbs;
    }

    /**
     * Reset the crumbs array to empty.
     * 
     * @return Breadcrumb
     */
    public function clear()
    {
        $this->crumbs = [];

        return $this;
    }

    /**
     * Reset the crumbs array to empty. Alias of clear()
     * 
     * @return Breadcrumb
     */
    public function reset()
    {
        return $this->clear();
    }

    public function display()
    {
        if (count($this->crumbs) == 0) {
            return;
        }
        $output = [];
        $output[] = '<' . $this->container_tag . ' class="' . $this->container_class . '">';
        foreach ($this->crumbs as $crumb) {
            if (isset($crumb['href'])) {
                $output[] = '<' . $this->item_tag . ' class="' . $this->item_class . '">';
                $output[] = '<a href="' . $crumb['href'] . '">' . $this->escape($crumb['text']) . '</a>';
                $output[] = '</' . $this->item_tag . '>';
            } else {
                $item_class = $this->active_class;
                if ($this->item_class) {
                    $item_class .= ' ' . $this->item_class;
                }
                $output[] = '<' . $this->item_tag . ' class="' . $item_class . '">';
                $output[] = $this->escape($crumb['text']);
                $output[] = '</' . $this->item_tag . '>';
            }
        }
        $output[] = '</' . $this->container_tag . '>';
        return implode(PHP_EOL, $output);
    }

    protected function escape($string)
    {
        return htmlspecialchars($string, ENT_COMPAT | ENT_HTML401, 'UTF-8');
    }

}
