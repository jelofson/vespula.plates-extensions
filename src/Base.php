<?php
namespace Vespula\PlatesExtensions;

use League\Plates\Extension\ExtensionInterface;
use Psr\Container\ContainerInterface;

abstract class Base implements ExtensionInterface 
{
	
    public $template;
	protected $engine;
	
    protected $container;

	
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}
}