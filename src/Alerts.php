<?php
namespace Vespula\PlatesExtensions;

use League\Plates\Engine;


class Alerts extends Base 
{
    protected $templates = [];
    protected $theme;
    
	public function register(Engine $engine)
	{
        $this->templates = include 'Alerts/templates.php';
		$engine->registerFunction('info', [$this, 'info']);
		$engine->registerFunction('error', [$this, 'error']);
		$engine->registerFunction('alerts', [$this, 'renderAlerts']);
	}
    
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    public function setTemplates($templates)
    {
        $this->templates = (array) $templates;
    }

    public function addToTemplates($data)
    {
        $this->templates = array_merge($data, $this->templates);
    }

	public function renderAlerts($messages, $theme = null)
	{
        $output= [];
		if (isset($messages['info']) && count($messages['info']) > 0) {
			foreach ($messages['info'] as $message) {
				$output[] = $this->decorate('info', $message, $theme); 
			}
		}
		
		if (isset($messages['error']) && count($messages['error']) > 0) {
			foreach ($messages['error'] as $message) {
				$output[] = $this->decorate('error', $message, $theme); 
			}
		}
		
		return implode("\n", $output);
	}
    
    public function info($message, $theme = null)
    {
        return $this->decorate('info', $message, $theme);
    }
    
    public function error($message, $theme = null)
    {
        
        return $this->decorate('error', $message, $theme);
    }
	
	protected function decorate($type, $message, $theme)
    {
        if (! $theme) {
            $theme = $this->theme;
        }
        if (array_key_exists($theme, $this->templates) && array_key_exists($type, $this->templates[$theme])) {
            return sprintf($this->templates[$theme][$type], $message);
        } else {
            return sprintf($this->templates['default'][$type], $message);
        }
        
    }
}
