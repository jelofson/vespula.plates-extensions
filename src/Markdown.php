<?php

namespace Vespula\PlatesExtensions;

use League\Plates\Engine;

class Markdown extends Base 
{
    protected $output;
    
    public function register(Engine $engine)
    {
        $this->engine = $engine;
        $engine->registerFunction('markdown', [$this, 'getSelf']);
    }
    
    public function getSelf($string)
    {
        $commonMark = $this->container->get('commonmark');
        $this->output = $commonMark->convertToHtml($string);
        return $this;
        
    }
    
    public function stripPara()
    {
        $this->output = str_replace(['<p>', '</p>'], ['', ''], $this->output);
        return $this;
    }
    
    public function strip()
    {
        $this->output = strip_tags($this->output);
        return $this;
    }
    
    public function __toString()
    {
        return (string) $this->output;
    }
}